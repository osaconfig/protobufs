// UserConfig Protocol Buffer - maps to openstack-deploy/openstack-user-config.yml
syntax = "proto3";
package osaconfig.userconfig;

service UserConfigService {
  rpc GetUserConfig(GetRequest) returns (Response) {}
  rpc CreateUserConfig(UserConfig) returns (Response) {}
  rpc RemoveUserConfig(UserConfig) returns (Response) {}
}

message UserConfig {
  string environment = 1;
  string description = 2;
  repeated CIDRNetwork cidr_networks = 3;
  repeated UsedIPs used_ips = 4;
  GlobalOverrides global_overrides = 5;
  repeated Anchor anchor_blocks = 6;
}

message CIDRNetwork {
  string id = 1;
  string cidr_network = 2;
  string cidr_netmask = 3;
}

message UsedIPs {
  string id = 1;
  string usedips_start = 2;
  string usedips_end = 3;
}

enum ContainerBridge {
  BRDUMMY = 0;
  BRMGMT = 1;
  BRVXLAN = 2;
  BRPROVIDER = 3;
  BRVLAN = 4;
  BRFLAT = 5;
  BRSTORAGE = 6;
  BRLBAAS = 7;
}

enum GroupBind {
  ALLCONTAINERS = 0;
  HOSTS = 1;
  NEUTRONOVNCONTROLLER = 2;
  NEUTRONLINUXBRIDGEAGENT = 3;
  UTILITYALL = 4;
  GLANCEAPI = 5;
  CINDERAPI = 6;
  CINDERVOLUME = 7;
  NOVACOMPUTE = 8;
  SWIFTPROXY = 9;
  CEPHOSD = 10;
  OCTAVIAWORKER = 11;
  OCTAVIAHOUSEKEEPING = 12;
  OCTAVIAHEALTHMONITOR = 13;
}

enum AnchorBlock {
  CINDER = 0;
  COMPUTE = 1;
  INFRA = 2;
  LOADBALANCER = 3;
  LOG = 4;
  SWIFT = 5;
  CEPHOSDS = 6;
}

message GlobalOverrides {
  string internal_lb_vip_address = 1;
  string external_lb_vip_address = 2;
  ContainerBridge tunnel_bridge = 3;
  ContainerBridge management_bridge = 4;
  message ProviderNetwork {
    ContainerBridge container_bridge = 1;
    string container_type = 2;
    string container_interface = 3;
    string network_interface = 4;
    string host_bind_override = 5;
    string ip_from_q = 6;
    string type = 7;
    repeated GroupBind group_binds = 8;
    bool is_container_address =9;
    bool is_ssh_address = 10;
    string range = 11;
    string net_name = 12;
  }
  repeated ProviderNetwork provider_networks = 5;
  // Swift Config
  message SwiftGlobalOverrides {
    string part_power = 1;
    ContainerBridge storage_network = 2;
    ContainerBridge replication_network = 3;
    message SwiftDrive {
      string name = 1;
    }
    repeated SwiftDrive drives= 4;
    string mount_point = 5;
    message StoragePolicy {
      string name = 1;
      uint32 index = 2;
      bool default = 3;
    }
    repeated StoragePolicy storage_policies = 6;
  }
  repeated SwiftGlobalOverrides swift_global_overrides = 6;
}


message Anchor {
  AnchorBlock anchor_name = 1;
  message Host {
    string host_name = 1;
    string ip = 2;
    repeated ContainerVars container_vars = 3;
  }
  repeated Host host = 2;
  bool enabled = 3;
}

message ContainerVars {
  string container_tech = 1;
  message StorageBackend {
    Cinder cinder_backend = 1;
    Ceph ceph_backend = 2;
    message Cinder {
      string limit_container_types = 1;
      message LVM {
        string volume_group = 1;
        string volume_driver = 2;
        string volume_backend_name = 3;
        string iscsi_ip_address = 4;
      }
      LVM lvm = 2;
    }
    message Ceph {
      message LVMVolumes {
        string data = 1;
        string data_vg = 2;
        string journal = 3;
        string journal_vg = 4;
      }
      LVMVolumes lvm_volumes = 1;
    }
  }
  StorageBackend storage_backend = 2;
}

message GetRequest {
  string environment = 1;
}

message Response {
  UserConfig userConfig = 1;
  bool created = 2;
  bool removed = 3;
}
