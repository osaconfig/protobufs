.PHONY: userconfig environment inventory variables

userconfig:
	protoc -I. --go_out=plugins=micro:$(GOPATH)/src/gitlab.com/osaconfig/protobufs \
		userconfig/userconfig.proto

environment:
	protoc -I. --go_out=plugins=micro:$(GOPATH)/src/gitlab.com/osaconfig/protobufs \
		envd/envd.proto

inventory:
	protoc -I. --go_out=plugins=micro:$(GOPATH)/src/gitlab.com/osaconfig/protobufs \
		inventory/inventory.proto

variables:
	protoc -I. --go_out=plugins=micro:$(GOPATH)/src/gitlab.com/osaconfig/protobufs \
		variables/variables.proto
